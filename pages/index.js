import { useState } from 'react'
import { useQuery } from 'react-query'
import Head from 'next/head'

import { getCounties } from '../app/data/index'

import CountiesSelect from '../app/components/countySelector'
import Header from '../app/components/header'
import Footer from '../app/components/footer'
import TotalCases from '../app/components/totalCases'
import NewCases from '../app/components/newCases'

const INITIAL_COUNTY_ID = '22-22103'
const GRAPH_TYPES = {
  TOTAL_CASES: 'TOTAL_CASES',
  NEW_CASES: 'NEW_CASES',
  INITIAL: 'TOTAL_CASES'
}

const Home = () => {
  const [selectedCounties, setSelectedCounties] = useState([INITIAL_COUNTY_ID])
  const [graphType, setGraphType] = useState(GRAPH_TYPES.INITIAL) // eslint-disable-line 

  const onSelectHandler = (value) => {
    if (value) {
      setSelectedCounties(value.map(v => v.id))
    } else {
      setSelectedCounties([])
    }
  }

  const { data: counties, status: countiesStatus } = useQuery('counties', getCounties)

  return (
    <>
      <Head>
        <title>G.K.&apos;s COVID Visualizer</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header />
      <div className="container p-4 mx-auto">
        {countiesStatus === 'success' &&
          <div className='mb-3 w-1/2 mx-auto'>
            <CountiesSelect counties={counties} onSelect={onSelectHandler} />
          </div>
        }
        {selectedCounties.length > 0
          ? <>
            <TotalCases selectedCounties={selectedCounties} />
            <NewCases selectedCounties={selectedCounties} />
          </>
          : <h2 className='text-center text-gray-600 text-2xl'>Nothing selected :(</h2>
        }
      </div>
      <Footer />
    </>
  )
}

export default Home
